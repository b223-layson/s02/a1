<?php require_once './code.php' ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Activity for Repetition Control Structures</title>
</head>

<body>


    <h1>Divisible of Five</h1>
    <?php modifiedForLoop(); ?>

    <h1>Array Manipulation</h1>
    <?php array_push($students, 'Johhn Smith'); ?>
    <p>
        <?php var_dump($students); ?>
    </p>
    <?php echo count($students); ?>



    <?php array_push($students, 'Jane Smith'); ?>
    <p>
        <?php var_dump($students); ?>
    </p>
    <?php echo count($students); ?>


    <pre>
        <?php array_shift($students); ?>
    </pre>
    <?php var_dump($students); ?>
    <?php echo count($students); ?>

</body>

</html>